.. rst2html5 README.rst > README.html
.. .. highlight:: bash
.. role:: prog(literal)
.. role:: envvar(literal)
.. role:: file(literal)

.. |PKGBUILD| replace:: ``PKGBUILD``
.. |ArchBuilder| replace:: :prog:`archbuilder`


%%%%%%%%%%%
ArchBuilder
%%%%%%%%%%%

Use to build in batch a series of PKGBUILD files.

Installer |ArchBuilder|
-----------------------

::

  git clone git@gitlab.com/elionne/archbuilder.git

|ArchBuilder| est un script ruby et a besoin de la gem clamp.

Créer un environnement avant d’utiliser |ArchBuilder|
-----------------------------------------------------

Le programme |ArchBuilder| a besoin d’une hierarchie de répertoire précise.
Dans la suite du document, tous les répertoires sont nommé par rapport à une racine choisie appelée :envvar:`ARCHBUILDER`.
Dans notre exemple :envvar:`ARCHBUILDER` est définie dans le répertoire :file:`~/archlinux`.

::

  ~/archlinux ($ARCHBUILDER)
  ├─ build ($CHROOT)
  │  └─ root
  │
  └─ packages ($OUTPUT)
     └─ custom
        ├─ custom.db.tar.gz
        └─ custom.db
              …

Dans la suite de ce document le chemin racine du *chroot* sera :file:`$ARCHBUILDER/build` et sera nommé :envvar:`CHROOT`.

Le répertoire de destination des paquets, et donc le dépôt temporaire (vide) se trouve dans le répertoire :file:`$ARCHBUILDER/packages/custom` appelé :envvar:`OUTPUT`.

.. code:: bash

  export ARCHBUILDER=~/archlinux
  export CHROOT=$ARCHBUILDER/build
  export OUTPUT=$ARCHBUILDER/packages

Les répertoires :envvar:`$CHROOT`, :envvar:`$OUTPUT`, :envvar:`$OUTPUT/custom` doivent être créés manuellement.

.. code:: bash

  mkdir -p $CHROOT $OUTPUT/custom

Personnalisation de la construction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour l’instant |ArchBuilder| ne gère pas personnalisation de la configuration de :prog:`makepkg`.
Il faut donc renseigner manuellement les informations dans le fichier :file:`~/.makepkg.conf`.

Remplir le fichier :file:`~/.makepkg.conf` comme suit.
Remplacer les champs nécessaire ::

   # The same as ${OUTPUT} environnement variable.
   PKGDEST=/home/jdoe/archlinux/packages/custom

   PACKAGER="John Doe <john.doe@example.com>"
   BUILDENV+=(!check)
   MAKEFLAGS="-j4"

Compiler dans un environnement propre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Voir `Building in a Clean Chroot <https://wiki.archlinux.org/index.php/DeveloperWiki:Building_in_a_Clean_Chroot>`_ pour tous les détails.

.. note::

  Pour les systèmes de fichiers en *btrfs*, le paquets ``btrfs-progs`` est indispensable.

La commande :prog:`mkarchroot` permet de créer un système de base archlinux qui sera utilisé pour compiler les paquets.

Un système de base doit être créé dans le répertoire :file:`$CHROOT/root`.
Le :file:`pacman.conf` dépend de votre besoin.

.. code:: bash

    mkarchroot $CHROOT/root -C pacman.conf base-devel git

Modification du pacman.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans le fichier :file:`$CHROOT/root/etc/pacman.conf` ajouter **avant** les autres dépôts, les lignes suivantes ::

  [custom]
  SigLevel = Optional TrustAll
  Server = file:///var/local/custom

Dépôt temporaire des paquets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Une fois le système de compilation créé, il faut maintenant créer un dépôt temporaire dans le lequel les paquets compilés seront ajoutés.

Se rendre dans le répertoire :envvar:`OUTPUT` :

.. code:: bash

  cd $OUTPUT/custom
  repo-add custom.db.tar.gz

Lancer la compilation automatique
---------------------------------

Dernier point la liste des |PKGBUILD| est fournie sous forme de répertoire.
Pour l’instant le répertoire doit avoir la hierarchie suivante ::

  .../sources ($PKGBUILDs)
  ├─ pkg_name1
  │  └─ PKGBUILD
  │  …
  │
  └─ pkg_name2
     └─ PKGBUILD

Le programme |ArchBuilder| peut maintenant être utilisé.

.. code:: bash

  bin/archbuilder.rb --buildroot $CHROOT --output $OUTPUT/custom $PKGBUILDs
