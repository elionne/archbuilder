#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

SUDO_USER = ENV["SUDO_USER"]

# Ensure this script is running from "sudo" to avoid asking password if
# compilation for one packet is too long.
exec(ENV, "sudo", "-E", RbConfig.ruby, __FILE__, *$*) unless Process.uid.zero?

require 'archbuilder/cli'

ArchBuilder::CLI.run

#### Topics

## Test if key is present in local gnupg data base before downloading it.
## Ensure `pacman` database is upto date. (pacman -Sy)
## Download makedepends with pacman in cache in advance
## Can use with a proxy
## Can be used without dependencies testing
## Use pretty log
## Use a config file or command line option
## Bug when source file contains spaces
## use arch-nspwan or docker
## rewrite makechrootpkg inside this app
## add clean sub command to remove all build files in ABSROOT
## can choose the repo name instead of 'custom'
## Only one option for all directory (buildroot, output and ABSROOT)
## Can get PKGBUILD from aur or abs and download dependencies
