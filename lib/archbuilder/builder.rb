require 'fileutils'
require 'archbuilder/utils'
require 'archbuilder/pkgbuild'
require 'archbuilder/tasker'


module ArchBuilder
  class Builder

    include ArchBuilder::Utils

    attr_reader :buildroot, :outputdir

    def initialize buildroot, outputdir
      @outputdir = outputdir
      @buildroot = buildroot
    end

    def download_sources(pkg, verify = true)
      FileUtils::cd pkg do
        makepkg '-o', '--verifysource', verify ? nil : '--skipinteg'
      end
    end

    def make_package(pkgbuild)
      arch_nspawn (buildroot/'root').to_s, '--bind-ro', "#{File.dirname(outputdir)}:/var/local", 'pacman', '-Sy'

      FileUtils::cd pkgbuild.dirname do
        makechrootpkg buildroot, :outputdir => outputdir
      end

      FileUtils::cd outputdir do
        repo_add 'custom.db.tar.gz'.to_s, pkgbuild["x86_64"].packages_with_ext
      end

    end

    ## GnuPG management
    def fetch_all_pgp_keys(pkg_list)
      all_key_id = pkg_list.flat_map{ |pkg| retrieve_pgp_keys pkg }.compact

      # To avoid use gpg --recv-keys multi times on the same key, we get keys
      # from all ask PKGBUILD and we construct a uniq key id list.
      #
      sh 'gpg', '--recv-keys', all_key_id.compact.uniq
    end

    def retrieve_pgp_keys(path)
        create_pkgbuild(path).validpgpkeys.simplify
    end

    def clean_cache(packages)
      pkg_to_clean = packages.map{|pkg| pkg["x86_64"].packages_with_ext }.flatten

      # Get DB path from pacman verbose mode
      cache_path = Path.new %x(pacman -Tv).match(/Cache.*/).to_s.split(':').last.strip

      FileUtils::rm_f pkg_to_clean.map{|name| cache_path/name}
    end

    def create_pkgbuild(path)
      pkgbuild = Pkgbuild.new path/Pkgbuild::PKGBUILD
      pkgbuild["x86_64"]
    end

    def create_all_pkgbuild packages
      packages.map do |pkg|
        puts pkg
        create_pkgbuild pkg
      end
    end

  end
end
