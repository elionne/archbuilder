
module ArchBuilder
  module Utils
    protected

    # From fpm. (lib/fpm/util.rb)
    def safesystem(*args)
      # Make sure to avoid nil elements in args. This might happen on 1.8.
      success = system(*args.compact.flatten)
      if !success
        raise "'system(#{args.inspect})' failed with error code: #{$?.exitstatus}"
      end
      return success
    end

    # Alias for safesystem.
    def sh(*args)
      safesystem(*args)
    end

    # from http://stackoverflow.com/questions/17044325/use-bin-bash-for-x-in-ruby
    #
    # Return the output of the given script. Running with bash instead of sh.
    def bash_with_output(script, *args)
      IO.popen(["/bin/bash", script, *args]) {|io| io.read}
    end

    def makepkg(*args)
      # Always run makepkg as user
      run_as_user 'makepkg', *args
    end

    def makechrootpkg(chroot, options)
      outputdir = File.dirname(options[:outputdir]) || FileUtils::pwd
      clean     = options[:clean] || true
      safesystem 'makechrootpkg', '-r', chroot.to_s, clean ? '-c' : nil, "-D", "#{outputdir}:/var/local",  "--"
    end

    def pacman(*args)
      system('pacman', *args, [:out, :err] => "/dev/null")
    end

    def repo_add *args
      run_as_user 'repo-add', *args
    end

    def arch_nspawn *args
      safesystem 'arch-nspawn', *args
    end

    def run_as_user *args
      safesystem 'sudo', '-u', ENV["SUDO_USER"], *args
    end

  end
end
