require 'rake/task'
require 'rake/early_time'
require 'rake/late_time'
require 'archbuilder/utils'

module Rake
  class PacmanTask < Task

    include ArchBuilder::Utils

    def needed?
      ! exists?
    end

    def timestamp
      if exists?
        EARLY
      else
        LATE
      end
    end

    def exists?
      @exists ||= pacman "-Sp", name
    end

  end
end
