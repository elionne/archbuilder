require 'rake/task'
require 'rake/early_time'

module Rake
  class DependTask < Task

    def needed?
      @prerequisites.any? { |n| application[n, @scope].needed? }
    end

    def timestamp
      task = @prerequisites.max_by { |n| application[n, @scope].timestamp }
      application[task, @scope].timestamp || EARLY
    end

  end
end
