require 'archbuilder/utils'
require 'archbuilder/path'
require 'yaml'
require 'uri'

module ArchBuilder
  class Pkgbuild
    include Utils

    PKGBUILD = "PKGBUILD".freeze

    def initialize file
      @pkgbuild = file
    end

    # Returns only the path name without "PKGBUILD" of the current PKGBUILD file
    # treated (e.g. /home/user/abs/core/package_a/).
    #
    # The returned object is a ArchBuilder::Path
    def dirname
      Path.new(File.dirname @pkgbuild)
    end

    # Returns a new Pkgbuild object with only the selected architecture if
    # available.
    #
    # It is useful to avoid to specify architectur for all methods, also in some
    # methods the returned object is not an Array and can be easly manage.
    #
    # If the PKGBUILD is defined with "any", any architecture you specify, self
    # is returned. If you specify an architecture not available an Expection is
    # raised. If "any" is specified self is returned.
    #
    # If a block is given, the given block is called with instance_eval on the
    # fresh created object with selected architecture. So all call to Pkgbuild
    # methods will be restricted to the selected architecture. In this case the
    # output of this method is output of the block.
    #
    # select_arch is private but you can use the aliased [] public operator.
    def select_arch(arch, &block)
      arch_selected = nil

      case
      when pkgbuild['arch'].include?(arch)
        arch_selected = self.clone

        other = pkgbuild.dup
        other['arch'] = arch

        arch_selected.instance_variable_set("@pkgbuild_raw", other.freeze)
      when pkgbuild['arch'].include?("any")
        arch_selected = self
      when arch == "any"
        arch_selected = self
      else
        raise "architecture can only be one of #{pkgbuild['arch']}"
      end

      return arch_selected.instance_eval &block if block_given?
      arch_selected
    end

    alias :[] :select_arch
    private :select_arch

    # Returns the full path of the current PKGBUILD file currently treated.
    # E.g. /var/abs/core/package_a/PKGBUILD
    #
    # The returned object is an ArchBuilder::Path
    def path
      Path.new(@pkgbuild)
    end

    PKG_EXT = '.pkg.tar.xz'.freeze
    # Append to the given pkgname the full version number. Returns an Array if
    # each version depends on architecture available or a string if only one
    # architecture is specified.
    def make_version(name)
      epoch = pkgbuild['epoch'] && pkgbuild['epoch'] + ':'
      ver   = pkgbuild['pkgver']
      rel   = pkgbuild['pkgrel']
      arch  = pkgbuild['arch']

      # From pacman documentation Versions are describe as follow
      # pkgname-[epoch:]pkgver-pkgrel-arch
      version = Proc.new {|arch| "%s-%s%s-%s-%s" % [name, epoch, ver, rel, arch]}

      case arch
      when Array
        arch.map &version
      when String
        version.call arch
      end
    end

    # Returns a list of which package can be created by `makepkg` command with
    # architecture information, but without extenstion. It is equivalente of
    # a `makepkg --packagelist`
    #
    # By default this method returns packages name for all architectures, but
    # you can restrict the list by setting the arch name in argument.
    #
    # Corresponding to Archlinux Build System, only "x86_64" and "i686" are
    # available or "any" for all architectures.
    #
    # If no package `name` is provided all possible packages are returned.
    #
    # Package name returned look like bzip2-1.0.6-5-x86_64
    # Use `packages_with_ext` to get a list of package name with extension
    # (.pkg.tar.xz).
    def packages(name = nil, arch: "any")
      select_arch(arch) do
        case name
        when nil    then pkgname = ensure_array(pkgbuild['pkgname'])
        when String then pkgname = ensure_array(name)
        when Array  then pkgname = name.delete_if{ |o| o.nil? }
        end

        pkgname.map{ |pkgname| make_version(pkgname) }.flatten
      end
    end

    def packages_with_ext(name = nil, arch: "any")
      packages(name, :arch => arch).map{|p| p + PKG_EXT}
    end

    # make a method for each PKGBUILD attribute
    ['source',
     'validpgpkeys',
     'depends',
     'makedepends',
     'checkdepends',
     'arch',
     'pkgname'].each do |attr|
      define_method(attr) {ensure_array pkgbuild[attr]}
    end

    alias :sources :source

    def ensure_array obj
      [obj].flatten(1)
    end

    private :ensure_array

    # Return all sources file needed by PKGBUILD. Downloadable sources are also
    # included even if they are not yet downloaded. Sources file mean the
    # absolute path of the source with its name.
    #
    # To get the URL of the downloaded sources use `sources`.
    def sources_name
      sources.map{|src| dirname/extract_source_name(src)}
    end

    # A non safe way to get source filename from PKGBUILD#source list uri.
    def extract_source_name(src)

      # Conforming to Archlinux `source` field description a specific output
      # filename can be overrided using '::' before the source url.
      #
      # See https://wiki.archlinux.org/index.php/PKGBUILD#source
      *name, path = src.split '::'
      return name.first unless name.empty?

      uri = URI.parse(path)
      case uri.scheme
      when 'git', 'git+https', 'git+http'
        # Git repo exists if '.git/HEAD' file exists
        File.join(File.basename(uri.path, '.git'), 'HEAD')

      # TODO add missing CVS special parsing case (svg, hg, ...).
      else
        File.basename(uri.path)
      end
    end

    def pkgbuild
      return @pkgbuild_raw unless @pkgbuild_raw.nil?

      yaml = bash_with_output "./lib/archbuilder/helper/makepkg.sh", @pkgbuild.to_s
      @pkgbuild_raw = YAML.load(yaml).freeze
    end

    private :pkgbuild

  end
end
