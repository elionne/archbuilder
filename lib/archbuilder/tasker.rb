require 'rake/file_task'
require 'rake/task'
require 'archbuilder/expand/array'
require 'archbuilder/rake/pacman_task'
require 'archbuilder/rake/depend_task'

module ArchBuilder
  class Tasker
    attr_reader :options


    def initialize(builder, options = {})
      @builder = builder
      @options = options

      @download_task = Rake::Task.define_task :download
      @build_task    = Rake::Task.define_task :build
      Rake::Task.define_task :default => [:download, :build]
    end


    # Create a FileTask for all sources which download the source. Download source
    # are made with `makepkg` for now.
    #
    # If a source file is missing Rake will run the `makepkg` to download file.
    # The output is an Array of path for all sources, that represent all FileTask,
    # so this list can be used as prerequists for other tasks.
    def create_download_sources_task(pkgbuild)
      sources = pkgbuild.sources_name
      sources.each do |s|
        Rake::FileTask.define_task(s) do
          @builder.download_sources pkgbuild.dirname, options[:verify]
        end
      end

      @download_task.enhance sources
    end

    # Create a build Task for the given pkgbuild.
    #
    # Create a FileTask for all final packages output file. These tasks depends
    # on all sources extracted from PKGBUILD file and PKGBUILD file itself.
    #
    # Returns an Array of full path of output packages files, like:
    #
    # ["/home/user/abs/core/bash-4.3.046-1-x86_64.pkg.tar.xz"]
    def create_package_build_task(pkgbuild)
      packages_out = []

      pkgbuild.pkgname.each do |pkgname|
        package_file = @builder.outputdir/pkgbuild.packages_with_ext(pkgname).first
        packages_out << package_file

        # Because of `makepkg` cannot build only one package we cannot separate
        # builds of each packages. But rake is smart enougth to not re-run block
        # if output file is already built.
        task = Rake::FileTask.define_task package_file do
          @builder.make_package pkgbuild
        end

        # OPTION: enhance task with all sources (PKGBUILD file, download files,
        # ...
        # task.enhance [pkgbuild.sources_name, pkgbuild.path, pkg_depends(pkgbuild)].flatten
        task.enhance [pkg_depends(pkgbuild)].flatten
      end

      packages_out
    end

    # Depends tasks are use to link tasks in the order they need to be built
    # properly. For example if bash depends on glibc, first build glibc then
    # bash.
    def pkg_depends(pkgbuild)
      depends = pkgbuild.depends + pkgbuild.makedepends + pkgbuild.checkdepends
      depends.flatten            \
        .delete_if{ |d| d.nil? } \
        .map{ |d| d.split(/(>=|<=|>|<|=)/).first }
    end

    # Create the main tasks build all packages of the current PKGBUILD.
    #
    # Those tasks does nothing, it just has prerequists all dependencies. Tasks
    # names are just de package name, not package filename.
    #
    # Returns an Array of all packages tasks.
    def create_main_tasks(pkgbuild)
      pkgbuild.pkgname.map do |pkgname|
        Rake::DependTask.define_task pkgname => @builder.outputdir/pkgbuild.packages_with_ext(pkgname).first
      end
    end

    def create_package_tasks(packages)
      packages.each do |pkgbuild|

        create_package_build_task(pkgbuild)
        create_download_sources_task(pkgbuild)
        pkgname = create_main_tasks(pkgbuild)

        @build_task.enhance pkgname
      end

      @build_task
    end

    def list_undefined_tasks
      all_asked_tasks = Rake.application.tasks.flat_map {|task| task.prerequisites}.uniq
      all_asked_tasks.delete_if do |task|
        Rake::Task.task_defined?(task) || task =~ /PKGBUILD/
      end
    end

    def invoke_rake(tasks)
      undef_tasks = list_undefined_tasks

      # OPTION: do nothing for missing task
      #undef_tasks.each{|task| Rake::Task.define_task(task) {} }

      # OPTION: check if misisng package exists in pacman database
      undef_tasks.each{|name| Rake::PacmanTask.define_task name}

      # OPTION: add missing task as dependency task
      #undef_tasks.each{|name| Rake::DependTask.define_task name}

      # OPTION: show prerequisites of tasks
      #Rake.application.options.show_prereqs = true

      # OPTION: show the order of build and why
      # Minial
      #Rake.application.options.dryrun = true
      # Or Full
      #Rake.application.options.trace = true

      # OPTION: build all
      #Rake.application.options.build_all = options[:build_all]

      Rake.application.collect_command_line_tasks tasks
      Rake.application.top_level
    end

    def build
      invoke_rake [:build]
    end

  end
end
