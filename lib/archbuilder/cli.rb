require 'clamp'
require 'rake'

require 'archbuilder/path'
require 'archbuilder/builder'


module ArchBuilder
  class CLI < Clamp::Command

    option '--download-only', :flag, 'download all sources from all packages then quit'
    option '--buildroot', 'DIR', 'directory where are stored containers for building packages',
        :environment_variable => "CHROOT", :required => true
    option '--output', 'DIR', 'directory for put all finished packages',
        :environment_variable => "OUTPUT", :required => true
    option '--fetch-gpg-keys', :flag, 'download all gpg keys into the wallet far makepkg signature verification'
    option '--skip-integrity', :flag, 'skip the checksum and signature for downloaded files'
    parameter 'ABSROOT', 'directory where are located all packages',
        :environment_variable => "ABSROOT"

    def execute
      # Get a list of all packages for ABSROOT
      #all_pkg = Path.new(absroot).each_child.flat_map{|m| m.each_child.to_a }
      all_pkg = Path.new(absroot).each_child.to_a

      # OPTION: Do no list hidden file (file start with '.')
      all_pkg = all_pkg.delete_if{ |r| File.basename(r).start_with? '.' }

      # OPTION: Use only directory
      all_pkg = all_pkg.delete_if{|r| not File.directory? r }

      # Build all needed package from given list
      builder = Builder.new(Path.new(buildroot), Path.new(output))
      tasker  = Tasker.new builder, :verify => !skip_integrity?

      pkgbuilds = builder.create_all_pkgbuild all_pkg
      tasker.create_package_tasks pkgbuilds

      case
        when fetch_gpg_keys?
          fetch_gpg_keys(builder, all_pkg)
        when download_only?
          download_only(tasker)
        else
          compile(tasker)
      end
    end

    def fetch_gpg_keys(builder, pkg)
      builder.fetch_all_pgp_keys pkg
    rescue
      exit 1
    end

    def compile(tasker)
      tasker.build
    rescue RuntimeError
      exit 1
    end

    def download_only(tasker)
      tasker.invoke_rake [:download]
    rescue RuntimeError
      exit 1
    end


  end
end
