
fields=(pkg{name,ver,rel,desc} epoch arch url license groups {make,check,}depends options validpgpkeys source)
checksums=({md5,sha1,sha256,sha512}sums)

print_array() {
    local attr=$1
    eval "keys=(\"\${${attr}[@]}\")"

    [ ${keys} ] || return
    printf "%s: [\"%s\"" ${attr} ${keys[0]}

    # Remove the first item in array
    keys=(${keys[@]:1})

    for v in ${keys[@]}; do
        printf ", \"%s\"" ${v}
    done
    printf "]\n"
}

print_attr() {
    local attr=$1
    printf "%s: \"%s\"\n" ${attr} "${!attr}"
}

print_key() {
    local key=$1
    eval "value=(\"\${$1[@]}\")"

    if [ ${#value[@]} -ge 2 ]; then
        print_array ${key};
    elif [ ${#value[@]} -ge 1 ]; then
        print_attr ${key};
    fi
}

print_yaml() {
    source $1

    echo "---"

    # print standards fields
    for f in ${fields[@]}; do
        print_key $f
    done

    # print checksums
    for sum in ${checksums[@]}; do
        [ ${sum[@]} ] && print_array $sum;
    done

    echo "..."
}

for pkg in "$@" ; do
    # Encapsulate the "source" in subshell.
    (print_yaml ${pkg})
done







