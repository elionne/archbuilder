class Array
  def simplify
    case self.length
    when 0 then return nil
    when 1 then return self.first
    else return self
    end
  end
end
