require 'rspec/collection_matchers'
require 'archbuilder/pkgbuild'

describe ArchBuilder::Pkgbuild do

  let(:pkgbuild) {described_class.new fixtures_path("PKGBUILD")}

  describe "#packages" do
    it "returns all packages" do
      expect(pkgbuild.packages).to include a_string_matching "bash"
      expect(pkgbuild.packages).to have(4).items
    end

    it "returns only arch for all packages" do
      expect(pkgbuild.packages(:arch => "any")).to have(4).items
      expect(pkgbuild.packages(:arch => "x86_64")).to have(2).items
      expect(pkgbuild.packages(:arch => "x86_64")).to include a_string_matching "x86_64"
      expect(pkgbuild.packages(:arch => "x86_64")).not_to include a_string_matching "i686"
    end

    it "returns only arch for specified packages" do
      expect(pkgbuild.packages('bash')).to have(2).items
      expect(pkgbuild.packages('bash')).to include a_string_matching "x86_64"
      expect(pkgbuild.packages('bash')).not_to include a_string_matching "abash"
      expect(pkgbuild.packages('bash', :arch => "x86_64")).not_to include a_string_matching "i686"
      expect(pkgbuild.packages('bash', :arch => "x86_64")).not_to include a_string_matching "abash"
    end
  end

  describe "#select_arch" do
    it "select only on architecture" do
      expect(pkgbuild["x86_64"].packages).to have(2).items
      expect(pkgbuild["x86_64"].packages).to include a_string_matching "x86_64"
      expect(pkgbuild["x86_64"].packages).not_to include a_string_matching "i686"
    end

    it "#select_arch is not public" do
      expect{pkgbuild.select_arch("x86_64").packages}.to raise_error NoMethodError
    end

    it "raise error if wrong arch" do
      expect{pkgbuild["arm"].packages}.to raise_error RuntimeError
    end
  end

  describe "#sources" do
    it "return all sources" do
      expect(pkgbuild.sources).to have(101).items
      expect(pkgbuild.sources).to include(a_string_matching("dot.bashrc"), a_string_matching("http://ftp.gnu.org/gnu/bash/bash-4.3-patches/bash43-001"))
    end
  end

  describe "#validpgpkeys" do
    it "return valid pgpkeys" do
      expect(pkgbuild.validpgpkeys).to eq ["7C0135FB088AAF6C66C650B9BB5869F064EA74AB"]
    end
  end

end
