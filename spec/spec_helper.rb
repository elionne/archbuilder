#$app_root = File.expand_path "../../lib", __FILE__
$spec_root = File.expand_path "..", __FILE__
#$LOAD_PATH.push $app_root

def fixtures_path(path)
  File.join($spec_root, "fixtures", path)
end
