Récupérer toutes les dépendences (récursives) d’un paquet donné
===============================================================

.. highlight:: bash

::

    pactree -u <nom_du_paquet>

Pour afficher le nom du paquet avec son dépôt (repo) ::

    pacman -Sp --print-format %r/%n $(pactree -u <nom_du_paquet>)

Récupérer avec |abs|_ une liste de paquets
==========================================

.. |abs| replace:: :emphasis:`abs`
.. _abs: https://wiki.archlinux.org/index.php/Arch_Build_System

Installer |abs| avec ::

    pacman -S abs

Configurer le répèrtoire racine de l’arborescence ABS.
J’ai choisi :file:`$HOME/archlinux/abs`.
Soit en modifiant dans le fichier global :file:`/etc/abs.conf` la variable :envvar:`ABSROOT`, soit avec une variable d’environement :envvar:`ABSROOT`.

::

    export ABSROOT=~/archlinux/abs
    # puis récupérer les paquets
    abs repo/package

Donc pour récupérer toutes les dépendances d’un paquets donné :

::

    abs $(pacman -Sp --print-format %r/%n $(pactree -u <nom_du_paquet>))


Créer l’environement avant de compiler les paquets
==================================================

Voir `Building in a Clean Chroot <https://wiki.archlinux.org/index.php/DeveloperWiki:Building_in_a_Clean_Chroot>`_ pour tous les détails.

La compilation des paquets doit se faire dans un environement propre.
Les scripts ArchLinux utilisent `systemd-nspawn <https://www.freedesktop.org/software/systemd/man/systemd-nspawn.html>`_ pour créer l’équivalent d’un chroot.

La commande ``mkarchroot`` permet de créer un système de base archlinux.
Ce script utilise ``pacstrap``.

Dans la suite de ce document le chemin racine du *chroot* sera :file:`~/archlinux/build` et sera nommé :envvar:`$CHROOT`.

Un système de base est créé dans le répertoire :file:`$CHROOT/root`.
Ce système contient le minimum de paquets pour pouvoir compiler, c’est-à-dire le groupe *base-devel*

::

    export CHROOT=~/archlinux
    mkarchroot $CHROOT/root base-devel

Lors de la compilation du paquet le script ``makechrootpkg`` copie le contenu de :file:`$CHROOT/root` dans un autre répertoire pour garder une copie toujours propre du sytème de base (*COPYDIR*).
Cela évite de recréer le systeme de base à chaque compilation.
La copie se fait par défaut dans le répertoire :file:`$CHROOT/$USER`, c’est le répertoire :envvar:`$COPYDIR`.
Sur les systèmes de fichiers *btrfs* le répertoire :file:`$CHROOT/root` est un *subvolume btrfs* et la copie est un *snapshot* crée depuis le répertoire :file:`$CHROOT/root`.

Si le script ``makechrootpkg`` est appelé avec l’option *-c* le répertoire :envvar:`$COPYDIR` est supprimé, puis recréé. D’où l’avantage avec le système de fichiers *btrfs*, les commandes suivantes sont appelées en interne :

::

    btrfs subvolume delete $COPYDIR
    btrfs subvolume snapshot $CHROOT/root $COPYDIR

Le fichier :file:`.makepkg.conf` est utilisé par le script ``makechrootpkg`` pour ajouter les variables non définit dans le fichier :file:`/etc/makepkg.conf`.

Pour que tous les paquets compilés soient copiés dans un répertoire unique, il faut ajouter au fichier :file:`.makepkg.conf` les lignes suivantes::

    PKGDEST=/home/jdoe/archlinux/packages
    PACKAGER="John Doe <john@doe.com>

Pour compiler un paquet, il faut se rendre dans le répèrtoire où se trouve le fichier |PKGBUILD| ::

    makechrootpkg -c -r $CHROOT


Créer les répertoires de build sur la ram
-----------------------------------------

https://gist.github.com/jheusala/1672448

Générer la base de données pour un mirroir local
================================================

::

   repo-add repo_name.db.tar.gz *.pkg.tar.xz

Automatiser la compilation des paquets
======================================

Le script |archbuilder| permet de créer automatiquement tous les paquets depuis les sources.

Compiler tous les paquets du répertoire avec |ArchBuilder|
==========================================================

.. |ArchBuilder| replace:: :program:`archbuilder`

On utilise tout le travail des développeurs ArchLinux pour la compilation des paquets.
Il faut donc avoir un dépôt synchronisé avec le `ArchLinux Build System`_.

.. _ArchLinux Build System: https://wiki.archlinux.org/index.php/Arch_Build_System

Une liste de paquets minimale peut être créée avec la commande bash suivante ::

  for package in $(pacman -Qqg base); do pactree -l $package; done | sort -u

|ArchBuilder| permet de compiler les paquets depuis les sources (|PKGBUILD|) contenues dans un répertoire.
Le script gère les dépendences des paquets.

Si pour une compilation, une dépendences est nécessaire le script va dans un premier temps regarder dans la liste des sources fournit si le paquet existe, si oui il est alors compilé avant puis ajouter à un dépôt temporaire. Sinon le paquet est téléchargé depuis les dépôts officiel ArchLinux.

Pour la compilation des paquets on utilise les scripts ArchLinux, *makepkg*, *makechroot*, ... .
Donc un système de base doit être installé avec notamment la group **base-devel**.

Rake pour bien faire
====================

Les paquets sont compilés uniquement si les fichiers de sorties doivent être créés.
Pour faciliter la tâche on utile la bibliothèque Ruby ``Rake``.

Pour chaque paquets on créé des *tasks* (tâches) avec les dépendences suivantes ::

  pkgfile -> pkgname -> sources
                     -> depends

Et ce pour chaque paquet.
Si un PKGBUILD contient plusieurs paquets, autant de *tasks* seront créées.

::

  pkgfile1 -> pkgname1 -> sources
                       -> depends

  pkgfile2 -> pkgname2 -> sources
                       -> depends

Tâches de download
------------------

Pour chaque sources à télécharger une tâche est également créée.
Toutes les tâches réseaux peuvent être téléchargés avant la compilation.


|PKGBUILD| (bash) |to| RUBY
===========================

.. |to| unicode:: 0x2799

Pour récupérer toutes les informations d’un fichier PKGBUILD j’utilise un convertisseur *bash* vers |YAML|_.
Le fichier :file:`lib/archbuilder/helper/pkgbuild.sh` permet de faire cette convertion.
Le script peut être utilisé de manière autonome, il accepte plusieurs PKGBUILD en argument.

Seuls les paramètres suivants sont convertis :

 - pkgname,
 - pkgver,
 - pkgrel,
 - pkgdesc,
 - epoch,
 - arch,
 - url,
 - license,
 - groups,
 - makedepends,
 - checkdepends,
 - depends,
 - options,
 - validpgpkeys,
 - source,
 - md5sums,
 - sha1sums,
 - sha256sums,
 - sha512sums.

.. |YAML| replace:: :abbr:`YAML (YAML Ain't Markup Language)`
.. _YAML: https://fr.wikipedia.org/wiki/YAML


Form scratch
============

Construit une distribution ArchLinux complète depuis zéro.
"Depuis Zéro" signifit vouloir construire tous les paquets sans aucuns paquets officiel pré-compilés.

Pour l’instant ArchBuilder ne gérère pas la contruction vraiment *from scratch* puisque des paquets pré-compilés sont nécessaires à cause de certaines dépendances circulaires.

Dépendences circulaires
-----------------------

.. warning::

  À compléter

- https://bbs.archlinux.org/viewtopic.php?id=203894

- http://www.linuxfromscratch.org/lfs/

Vrai *from scratch*.

Stage 1
^^^^^^^

Compilation minimale d’une suite de paquets pour la compilation du stage 2.

Stage 2
^^^^^^^

Compilation standard des paquets ArchLinux.


.. |PKGBUILD| replace:: :file:`PKGBUILD`


Cross compilation
=================

Pour compiler des paquets vers une autre architecture, arm par exemple, il existe une solution.

Créer avec pacstrap un root pour l’architecture cible ::

  pacstrap -C arm-pacman.conf -d -G -M arm_root

Le fichier *arm-pacman.conf* peut être récupérer depuis le projet `archlinuxarm.org`_.

.. _archlinuxarm.org: http://archlinuxarm.org

chaine de compilation croisé
----------------------------

L’outil ``crosstool-ng`` permet de créer des chaines de compilation croisé pour différentes cibles.

Se reporter à la documentation.

Créer le paquets de la chaine de compilation croisée
----------------------------------------------------


